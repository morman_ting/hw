using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace hw.Models
{
    #region New Code
    public class DepartmentInsertResult
    {
        public int DepartmentId { get; set; }
        public byte[] RowVersion { get; set; }
    }
    #endregion
}