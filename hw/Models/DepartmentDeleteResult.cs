using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace hw.Models
{
    #region New Code
    public class DepartmentDeleteResult
    {
        public int DepartmentId { get; set; }
        public byte[] RowVersion { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsDeleted { get; set; }
    }
    #endregion
}