using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace hw.Models
{
    #region New Code
    public class DepartmentUpdateResult
    {
        public byte[] RowVersion { get; set; }
        public DateTime DateModified { get; set; }
    }
    #endregion
}