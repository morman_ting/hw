USE [ContosoUniversity]
GO

/****** Object:  StoredProcedure [dbo].[Department_Delete]    Script Date: 2020/8/15 上午 09:33:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Department_Delete]
    @DepartmentID [int],
    @RowVersion_Original [rowversion]
AS
BEGIN
    UPDATE [dbo].[Department]
    SET [DateModified] = GETDATE(), [IsDeleted] = 1
    WHERE (([DepartmentID] = @DepartmentID) AND (([RowVersion] = @RowVersion_Original) OR ([RowVersion] IS NULL AND @RowVersion_Original IS NULL)))

	SELECT t0.[DepartmentID], t0.[RowVersion], t0.[DateModified], t0.[IsDeleted]
    FROM [dbo].[Department] AS t0
    WHERE @@ROWCOUNT > 0 AND t0.[DepartmentID] = @DepartmentID
END


GO


