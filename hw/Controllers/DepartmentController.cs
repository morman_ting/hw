using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using hw.Models;

namespace hw.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly ContosouniversityContext _context;

        public DepartmentController(ContosouniversityContext context)
        {
            _context = context;
        }

        // GET: api/Department
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Department>>> GetDepartment()
        {
            return await _context.Department.ToListAsync();
        }

        // GET: api/Department/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Department>> GetDepartment(int id)
        {
            var department = await _context.Department.FindAsync(id);

            if (department == null)
            {
                return NotFound();
            }

            return department;
        }

        // PUT: api/Department/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> PutDepartment(int id, Department department)
        {
            #region Old Code
            // if (id != department.DepartmentId)
            // {
            //     return BadRequest();
            // }

            // _context.Entry(department).State = EntityState.Modified;

            // try
            // {
            //     await _context.SaveChangesAsync();
            // }
            // catch (DbUpdateConcurrencyException)
            // {
            //     if (!DepartmentExists(id))
            //     {
            //         return NotFound();
            //     }
            //     else
            //     {
            //         throw;
            //     }
            // }

            // return NoContent();
            #endregion

            #region New Code
            if (id != department.DepartmentId)
            {
                return BadRequest();
            }

            try
            {
                var _result = await _context.DepartmentUpdateResult.FromSqlRaw(
                    "EXEC dbo.Department_Update @DepartmentID = {0}, @Name = {1}, @Budget = {2}, @StartDate = {3}, @InstructorID = {4}, @RowVersion_Original = {5};",
                    department.DepartmentId,
                    department.Name,
                    department.Budget,
                    department.StartDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    department.InstructorId,
                    department.RowVersion
                ).ToListAsync();
            }
            catch (System.Exception)
            {
                
                throw;
            }

            return NoContent();
            #endregion
        }

        // POST: api/Department
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Department>> PostDepartment(Department department)
        {
            #region Old Code
            // _context.Department.Add(department);
            // await _context.SaveChangesAsync();

            // return CreatedAtAction("GetDepartment", new { id = department.DepartmentId }, department);
            #endregion

            #region New Code
            List<DepartmentInsertResult> _result;

            try
            {
                _result = await _context.DepartmentInsertResult.FromSqlRaw(
                    "EXEC dbo.Department_Insert @Name = {0}, @Budget = {1}, @StartDate = {2}, @InstructorID = {3};",
                    department.Name,
                    department.Budget,
                    department.StartDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    department.InstructorId
                ).ToListAsync();

                department = await _context.Department.FindAsync(_result.FirstOrDefault().DepartmentId);
            }
            catch (System.Exception)
            {
                throw;
            }

            return CreatedAtAction("GetDepartment", new { id = department.DepartmentId }, department);
            #endregion
        }

        // DELETE: api/Department/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<Department>> DeleteDepartment(int id)
        {
            #region Old Code
            // var department = await _context.Department.FindAsync(id);
            // if (department == null)
            // {
            //     return NotFound();
            // }

            // _context.Department.Remove(department);
            // await _context.SaveChangesAsync();

            // return department;
            #endregion

            #region New Code
            var department = await _context.Department.FindAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            try
            {
                var _result = await _context.DepartmentDeleteResult.FromSqlRaw(
                    "EXEC dbo.Department_Delete @DepartmentID = {0}, @RowVersion_Original = {1};",
                    department.DepartmentId,
                    department.RowVersion
                ).ToListAsync();

                if (department.DepartmentId == _result.FirstOrDefault().DepartmentId)
                {
                    department.RowVersion = _result.FirstOrDefault().RowVersion;
                    department.DateModified = _result.FirstOrDefault().DateModified;
                    department.IsDeleted = _result.FirstOrDefault().IsDeleted;
                }

                return department;
            }
            catch (System.Exception)
            {
                
                throw new DbUpdateException();
            }

            #endregion
        }

        private bool DepartmentExists(int id)
        {
            return _context.Department.Any(e => e.DepartmentId == id);
        }

        #region New Code
        [HttpGet("~/api/DepartmentCourseCount")]
        public async Task<ActionResult<IEnumerable<VwDepartmentCourseCount>>> GetDepartmentCourseCount()
        {
            return await _context.VwDepartmentCourseCount.FromSqlRaw("SELECT [DepartmentID],[Name],[CourseCount] FROM [dbo].[vwDepartmentCourseCount];").ToListAsync();
        }
        #endregion
    }
}
